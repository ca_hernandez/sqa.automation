package clarityUnitTestScenarios;


import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import com.sequenom.common.ReportManager.ReportManager;
import com.sequenom.common.ReportManager.ReportManager.REPORT_STATUS;
import com.sequenom.common.Utilities.PropertiesObject;
import com.sequenom.Clarity.DataEngine.ClarityDataManager;
import com.sequenom.Clarity.DataEngine.ClarityDataManager.ClarityData;
import com.sequenom.Clarity.UI.Login.ClarityLoginPage;
import com.sequenom.Clarity.UI.Login.ClarityLoginPage.ClarityEnv;
import com.sequenom.Clarity.UI.Common.ClarityNavigation;
import com.sequenom.Clarity.UI.Common.ClarityNavigation.T21Protocol.Extraction.EXTRACTION_STEP;
import com.sequenom.Clarity.UI.Common.ClarityNavigation.T21Protocol.LibPrep.LIB_PREP_STEPS;
import com.sequenom.Clarity.UI.Common.ClarityNavigation.T21Protocol.LibQC.LIB_QC_STEPS;
import com.sequenom.Clarity.UI.Common.ClarityNavigation.T21Protocol.NormToDataAnalysis.NORM_TO_DA_STEPS;
import com.sequenom.Clarity.UI.Common.ClarityPlaceSamplesBasePage.CONTAINER;
import com.sequenom.Clarity.UI.Extraction.ExtractionAutomatedBuildBatchPage;
import com.sequenom.Clarity.UI.Extraction.ExtractionAutomatedPostRunValidationPage;
import com.sequenom.Clarity.UI.Extraction.ExtractionSampleBatchFailurePage;
import com.sequenom.Clarity.UI.Extraction.ExtractionSampleStatusAndMethodSelectionPage;
import com.sequenom.Clarity.UI.Extraction.ExtractionSampleStatusAndMethodSelectionPage.RecordDetailsPage.PlexingOption;
import com.sequenom.Clarity.UI.Extraction.ExtractionSampleStatusAndMethodSelectionPage.RecordDetailsPage.SampleStatus;
import com.sequenom.Clarity.UI.Extraction.ExtractionSampleStatusAndMethodSelectionPage.RecordDetailsPage.StatusCheck;
import com.sequenom.Clarity.UI.LibPrep.LibPrepBuildPlatePage;
import com.sequenom.Clarity.UI.LibPrep.LibPrepPCRPage;
import com.sequenom.Clarity.UI.LibPrep.LibPrepPostPCRPage;
import com.sequenom.Clarity.UI.LibPrep.LibPrepPrePCRPage;
import com.sequenom.Clarity.UI.LibPrep.LibPrepSampleBatchFailurePage;
import com.sequenom.Clarity.UI.LibPrep.LibPrepBuildPlatePage.ALLOWED_ORDER_CHOICE;
import com.sequenom.Clarity.UI.LibPrep.LibPrepBuildPlatePage.COC_METHOD;
import com.sequenom.Clarity.UI.LibQC.LibQC20xDilutionPage;
import com.sequenom.Clarity.UI.LibQC.LibQCChipLoadingPage;
import com.sequenom.Clarity.UI.LibQC.LibQCChipPrepPage;
import com.sequenom.Clarity.UI.LibQC.LibQCLabChipAnalysisSampleFailure;
import com.sequenom.Clarity.UI.NormalizationToDataAnalysis.DAPlateReportAnalysisPage;
import com.sequenom.Clarity.UI.NormalizationToDataAnalysis.DAImportClassificationFilePage.NextStepsPage.DA_IMPORT_FILE_NEXT_STEP;
import com.sequenom.Clarity.UI.NormalizationToDataAnalysis.DAPlateReportAnalysisPage.NextStepsPage.DATA_ANALYSIS_NEXT_STEP;
import com.sequenom.Clarity.UI.NormalizationToDataAnalysis.HOClusteringTypePage;
import com.sequenom.Clarity.UI.NormalizationToDataAnalysis.HOClusteringTypePage.RecordDetailsPage.FLOW_CELL_ASSIGNMENT_TYPE;
import com.sequenom.Clarity.UI.NormalizationToDataAnalysis.DAImportClassificationFilePage;
import com.sequenom.Clarity.UI.NormalizationToDataAnalysis.HOClusteringPage;
import com.sequenom.Clarity.UI.NormalizationToDataAnalysis.HOLoadFlowCellPage;
import com.sequenom.Clarity.UI.NormalizationToDataAnalysis.HOMultiplexingPage;
import com.sequenom.Clarity.UI.NormalizationToDataAnalysis.HOSequencingPage;
import com.sequenom.Clarity.UI.NormalizationToDataAnalysis.NormalizationPage;
import com.sequenom.Clarity.UI.NormalizationToDataAnalysis.SeqControlAnalysisPage;
import com.sequenom.Clarity.UI.NormalizationToDataAnalysis.SeqRunMonitoringFirstBaseReportPage;
import com.sequenom.Clarity.UI.NormalizationToDataAnalysis.SeqRunMonitoringPFClusterDensityPage;
import com.sequenom.Clarity.UI.NormalizationToDataAnalysis.HOMultiplexingPage.MULTIPLEXING_CRTLS;
import com.sequenom.Clarity.UI.NormalizationToDataAnalysis.SeqControlAnalysisPage.NextStepsPage.CTRL_ANALYSIS_NEXT_STEP;
import com.sequenom.Clarity.UI.NormalizationToDataAnalysis.SeqRunMonitoringFirstBaseReportPage.NextStepsPage.SEQ_FIRST_BASE_REPORT_NEXT_STEP;
import com.sequenom.Clarity.UI.NormalizationToDataAnalysis.PlexingAssignmentPage;
import com.sequenom.Clarity.UI.NormalizationToDataAnalysis.SeqRunMonitoringRawClusterDensityPage;
import com.sequenom.Clarity.UI.NormalizationToDataAnalysis.SeqRunMonitoringPFClusterDensityPage.NextStepsPage.SEQ_PF_CLUSTER_NEXT_STEP;
import com.sequenom.Clarity.UI.NormalizationToDataAnalysis.SeqRunMonitoringRawClusterDensityPage.NextStepsPage.SEQ_RAW_CLUSTER_NEXT_STEP;


public class Clarity_12PLEX_QA_StagingWorkflow{

	
	//ClarityDataManager ClarityDataManager = oDataMgr.`; 
	 
	  @BeforeTest
	  public void setUp()throws Exception {
		  
		  
		ReportManager.init();
		ClarityDataManager.Settings.setXMLDataSettingsFile(PropertiesObject.getPropertyValue("Clarity.DataSettings.XMLDataFileLocation"));
		ClarityDataManager.Settings.initXLSDataSettingsFile(PropertiesObject.getPropertyValue("Clarity.Settings.XLSFileLocation"),"EnvironmentConfiguration", 4);
		ClarityDataManager.Settings.setTestDataSourceFolder(PropertiesObject.getPropertyValue("Clarity.DataSettings.TestDataFolder"));
		ClarityDataManager.Settings.setTestDataOutputFolder(PropertiesObject.getPropertyValue("Clarity.DataSettings.TestDataOutputFolder"));
		ClarityDataManager.Settings.setXLSDataSourceFile(PropertiesObject.getPropertyValue("Clarity.DataSettings.XLSDataSource"));
		//ClarityDataManager.setProperty(ClarityData.EXTRACTION_PLATE, "TMP_CH01252016");
	  }

	  

	  @Test(priority= 1,description="Login to Clarity Application")
	  public  void Clarity_Login() throws Exception {
		    ReportManager.startTestCase("Clarity Login", "Login To Clarity"  + PropertiesObject.getPropertyValue("Clarity.Env.Default"));
		    ClarityLoginPage.StartAndLoginTo(ClarityEnv.QA_SD);
		  	ReportManager.endTestCase();
	  }

	  /*
	  
	  @Test(priority= 2, description="Extraction Protocol -Step 1", dependsOnMethods = { "Clarity_Login" })
	  public  void Clarity_Extraction() throws Exception {
		  ReportManager.startTestCase("Clarity-Extraction", "Clarity-Extraction");
		  
		  ClarityNavigation.T21Protocol.Extraction.step(EXTRACTION_STEP.SAMPLE_STATUS);
		  //ExtractionSampleStatusAndMethodSelectionPage.IceBucketExtractionPage.addItems(PropertiesObject.getPropertyValue("Clarity.DataSettings.DataSource"),"Extraction",1);
		  ExtractionSampleStatusAndMethodSelectionPage.IceBucketExtractionPage.addItems(ClarityDataManager.Settings.getXLSDataSourceFile(),"Extraction",1);
		  ExtractionSampleStatusAndMethodSelectionPage.VIEW_ICE_BUCKET();
		  ExtractionSampleStatusAndMethodSelectionPage.IceBucketContentsPage.setDestinationContainer();
		  ExtractionSampleStatusAndMethodSelectionPage.BEGIN_WORK();
		  //ExtractionSampleStatusAndMethodSelectionPage.PlaceSamplesPage.renameBucket("tmp_1234");
		  ExtractionSampleStatusAndMethodSelectionPage.PlaceSamplesPage.renameBucket();
		  ExtractionSampleStatusAndMethodSelectionPage.RECORD_DETAILS();
		  
		  
		  ExtractionSampleStatusAndMethodSelectionPage.RecordDetailsPage.fillUpPage(ClarityDataManager.Settings.getXLSDataSourceFile(),"Extraction_Sample_Status",1);
		  
		  //Overwriting the Plexing Option 
		  // 12-Plex 
		  // 24-Plex
		  //Genome Run
		  ExtractionSampleStatusAndMethodSelectionPage.RecordDetailsPage.setPlexChoice(PlexingOption.PLEX_12);
		  //T21
		  //ExtractionSampleStatusAndMethodSelectionPage.RecordDetailsPage.setPlexChoice(PlexingOption.PLEX_24);
		  
		  //ExtractionSampleStatusAndMethodSelectionPage.RecordDetailsPage.setExtractionMethod("Automated");
		  //ExtractionSampleStatusAndMethodSelectionPage.RecordDetailsPage.setOrderChoice("MIXED");
		  
		  //ExtractionSampleStatusAndMethodSelectionPage.RecordDetailsPage.checkSampleStatus(SampleStatus.SUCCESSFUL); 
		  ExtractionSampleStatusAndMethodSelectionPage.RecordDetailsPage.checkSampleStatus(SampleStatus.SUCCESSFUL);
		  ExtractionSampleStatusAndMethodSelectionPage.RecordDetailsPage.checkExtractionSampleStatus(StatusCheck.COMPLETED);
		  ExtractionSampleStatusAndMethodSelectionPage.NEXT_STEPS();
		  ExtractionSampleStatusAndMethodSelectionPage.FINISH_STEP();
		  ReportManager.endTestCase();
	  } 
	  
	  
	  
	  //@Test(priority= 2, description="Extraction Protocol -Step 2", dependsOnMethods = { "Clarity_Login" })
	   @Test(priority= 3, description="Automated Build Batch -Step 2" ,dependsOnMethods = { "Clarity_Extraction" })
	  //@Test(priority= 3, description="Automated Build Batch -Step 2" ,dependsOnMethods = { "Clarity_Extraction" })
	  public  void Clarity_Extraction_Automated_Build_Batch() throws Exception {
		  ReportManager.startTestCase("Clarity-Extraction Build Batch", "Adding Batch");
		  ClarityNavigation.T21Protocol.Extraction.step(EXTRACTION_STEP.AUTOMATED_BUILD);
		  ExtractionAutomatedBuildBatchPage.IceBucketExtractionPage.addItem(ClarityDataManager.getPropertyValue(ClarityData.EXTRACTION_PLATE));
		  ExtractionAutomatedBuildBatchPage.IceBucketExtractionPage.addNPP_And_NTC_Ctrls();
		  ExtractionAutomatedBuildBatchPage.VIEW_ICE_BUCKET();
		  
		  ExtractionAutomatedBuildBatchPage.IceBucketContentsPage.setDestinationContainer();
		  ExtractionAutomatedBuildBatchPage.BEGIN_WORK();
		  //ExtractionAutomatedBuildBatchPage.PlaceSamplesPage.renamePlateNameWithExistingValue("DNA_CH01042015");
		  ExtractionAutomatedBuildBatchPage.PlaceSamplesPage.renamePlateNameWithAutoValue();
		  ClarityDataManager.Extraction.FileGeneration.generateExtractionFiles("ExtractionFileConfiguration", 1,"Extraction");
		  ExtractionAutomatedBuildBatchPage.RECORD_DETAILS();
		  ExtractionAutomatedBuildBatchPage.RecordDetailsPage.fillUpPage(ClarityDataManager.Settings.getXLSDataSourceFile(),"ExtractionAutomatedBuildBatch",3);
		  ExtractionAutomatedBuildBatchPage.RecordDetailsPage.importFile();
		  ExtractionAutomatedBuildBatchPage.RecordDetailsPage.checkImportStatus();
		  ExtractionAutomatedBuildBatchPage.NEXT_STEPS();
		  ExtractionAutomatedBuildBatchPage.FINISH_STEP();
		  ReportManager.endTestCase();
	  } 
	  
	  
	   

	  //@Test(priority= 2, description="Extraction Protocol -Step 2", dependsOnMethods = { "Clarity_Login" })
	  @Test(priority= 4, description="Automated Build Batch -Step 3" ,dependsOnMethods = { "Clarity_Extraction_Automated_Build_Batch" })
	  public  void Clarity_Extraction_Automated_Post_Run_Validation() throws Exception {
		  ReportManager.startTestCase("Clarity-Extraction Build Batch", " Clarity-Extraction Build Batch");
		  ClarityNavigation.T21Protocol.Extraction.step(EXTRACTION_STEP.AUTOMATED_POST_RUN_VALIDATION);
		  ExtractionAutomatedPostRunValidationPage.IceBucketExtractionPage.addItem(ClarityDataManager.getPropertyValue(ClarityData.EXTRACTION_DNA_PLATE));  
		  ExtractionAutomatedPostRunValidationPage.VIEW_ICE_BUCKET();
		  ExtractionAutomatedPostRunValidationPage.BEGIN_WORK();
		  ExtractionAutomatedPostRunValidationPage.RecordDetailsPage.fillStepDetails();
		  ExtractionAutomatedPostRunValidationPage.RecordDetailsPage.validateOutputFile();
		  ExtractionAutomatedPostRunValidationPage.RecordDetailsPage.checkImportStatus();
		  ExtractionAutomatedPostRunValidationPage.NEXT_STEPS();
		  ExtractionAutomatedPostRunValidationPage.FINISH_STEP();
	  } 
	  

	  
	  
	  //@Test(priority= 2, description="Extraction_SampleBatch_Failure -Step 7", dependsOnMethods = { "Clarity_Login" })
	  @Test(priority= 5, description="Extraction_SampleBatch_Failure -Step 7" ,dependsOnMethods = { "Clarity_Extraction_Automated_Post_Run_Validation" })
	  public  void Clarity_Extraction_SampleBatch_Failure() throws Exception {
		  ReportManager.startTestCase("T21 Extraction: Sample / Batch Failure","Clarity_Extraction_SampleBatch_Failure");
		  	  ClarityNavigation.T21Protocol.Extraction.step(EXTRACTION_STEP.SAMPLE_BATCH_FAILURE);
			  ExtractionSampleBatchFailurePage.IceBucketExtractionPage.addItem(ClarityDataManager.getPropertyValue(ClarityData.EXTRACTION_DNA_PLATE));  
			  ExtractionSampleBatchFailurePage.VIEW_ICE_BUCKET();
			  ExtractionSampleBatchFailurePage.BEGIN_WORK();
			  ExtractionSampleBatchFailurePage.RecordDetailsPage.fillStepDetails();
			  ExtractionSampleBatchFailurePage.NEXT_STEPS();
			  ExtractionSampleBatchFailurePage.FINISH_STEP();
		  ReportManager.endTestCase();
	  }//Clarity_Extraction_SampleBatch_Failure
	  
	  
	  	  
	  
	  
	  //@Test(priority= 2, description="Lib Prep - Build Plate ", dependsOnMethods = { "Clarity_Login" })
	  @Test(priority= 6, description="Clarity LibPrep Build Plate -  Step 1" ,dependsOnMethods = { "Clarity_Extraction_SampleBatch_Failure" })
	  public  void Clarity_LibPrep_Build_Plate() throws Exception {
		  ReportManager.startTestCase("T21 Lib Prep","Clarity_LibPrep_Build_Plate");
		      ClarityNavigation.T21Protocol.LibPrep.step(LIB_PREP_STEPS.BUILD_PLATE);
		      LibPrepBuildPlatePage.IceBucketLibPrepPage.addItem(ClarityDataManager.getPropertyValue(ClarityData.EXTRACTION_DNA_PLATE));
			  LibPrepBuildPlatePage.VIEW_ICE_BUCKET();
			  //LibPrepBuildPlatePage.IceBucketContentsPage.setDestinationContainer();
			  LibPrepBuildPlatePage.BEGIN_WORK();
			  LibPrepBuildPlatePage.PlaceSamplesPage.renamePlateName();
			  //LibPrepBuildPlatePage.PlaceSamplesPage.renamePlateNameWithExistingValue();
			  LibPrepBuildPlatePage.RECORD_DETAILS();
			  LibPrepBuildPlatePage.RecordDetailsPage.fillUpPage(ClarityDataManager.Settings.getXLSDataSourceFile(),"LibPrep_Build_Plate",1);
			  //LibPrepBuildPlatePage.RecordDetailsPage.fillUpPage(ClarityDataManager.getPropertyValue(ClarityData.EXTRACTION_DNA_PLATE), ALLOWED_ORDER_CHOICE.MIXED, COC_METHOD.AUTOMATED);
			  LibPrepBuildPlatePage.RecordDetailsPage.buildLibraryPrepPlateRun();
			  LibPrepBuildPlatePage.RecordDetailsPage.checkLibPrepBuildPlateStatus();
			  LibPrepBuildPlatePage.RecordDetailsPage.checkLibPrepCOCStatus();
			  LibPrepBuildPlatePage.NEXT_STEPS();
			  //LibPrepBuildPlatePage.NextStepsPage.selectNextStep(sNextStep);
			   LibPrepBuildPlatePage.FINISH_STEP();
		  ReportManager.endTestCase();
	  }//Clarity_LibPrep_Build_Plate
	  
	  
	  
	  
	  //@Test(priority= 2, description="Lib Prep - Pre-PCR ", dependsOnMethods = { "Clarity_Login" })
	  @Test(priority= 7, description="Lib Prep - Pre-PCR" ,dependsOnMethods = { "Clarity_LibPrep_Build_Plate" })
	  public  void Clarity_LibPrep_Pre_PCR() throws Exception {
		  ReportManager.startTestCase("T21 Lib Prep: Pre-PCR","Clarity T21 Library Prep: Pre-PCR");
		      ClarityNavigation.T21Protocol.LibPrep.step(LIB_PREP_STEPS.PRE_PCR);
		      LibPrepPrePCRPage.IceBucketLibPrepPage.addItem(ClarityDataManager.getPropertyValue(ClarityData.LIB_PREP_DNA_PLATE));
			  LibPrepPrePCRPage.VIEW_ICE_BUCKET();
			  //LibPrepPrePCRPage.IceBucketContentsPage.setDestinationContainer();
			  LibPrepPrePCRPage.BEGIN_WORK();
			  LibPrepPrePCRPage.PlaceSamplesPage.renamePCRPlateNameWithAutoValue();
			  LibPrepPrePCRPage.RECORD_DETAILS();
			  LibPrepPrePCRPage.RecordDetailsPage.fillUpPage(ClarityDataManager.Settings.getXLSDataSourceFile(),"LibPrep_PRE_PCR",3);
			  ClarityDataManager.LibPrepPCR.FileGeneration.generateDNAFile("Lib_Prep_Configuration", 1);
			  
			  LibPrepPrePCRPage.RecordDetailsPage.validateZephyrFile();
			  LibPrepPrePCRPage.RecordDetailsPage.checkLibPrepPreCSVStatus();
			  LibPrepPrePCRPage.NEXT_STEPS();
			  //LibPrepBuildPlatePage.NextStepsPage.selectNextStep(sNextStep);
			   LibPrepBuildPlatePage.FINISH_STEP();
		  ReportManager.endTestCase();
	  }//Clarity_LibPrep_Pre_PCR
	  
	  
	 
	 //@Test(priority= 2, description="Lib Prep - Pre-PCR ", dependsOnMethods = { "Clarity_Login" })
	 @Test(priority= 8, description="Lib Prep - Pre-PCR" ,dependsOnMethods = { "Clarity_LibPrep_Pre_PCR" })
	  public  void Clarity_LibPrep_PCR() throws Exception {
		  ReportManager.startTestCase("T21 Lib Prep: PCR","Clarity T21 Library Prep: PCR");
		      ClarityNavigation.T21Protocol.LibPrep.step(LIB_PREP_STEPS.PCR);
		      LibPrepPCRPage.IceBucketLibPrepPage.addItem(ClarityDataManager.getPropertyValue(ClarityData.LIB_PREP_PCR_PLATE));
			  LibPrepPCRPage.VIEW_ICE_BUCKET();
			  LibPrepPCRPage.BEGIN_WORK();
			  //LibPrepPCRPage.RecordDetailsPage.fillUpPage("asda",CHECKBOX_VALUES.ON,"MY COMMENTS");
			  LibPrepPCRPage.RecordDetailsPage.fillUpPage();
			  LibPrepPCRPage.NEXT_STEPS();
			  //LibPrepBuildPlatePage.NextStepsPage.selectNextStep("");
			 LibPrepBuildPlatePage.FINISH_STEP();
		  ReportManager.endTestCase();
	  }//Clarity_LibPrep_PCR
	  
	 
	  
	  //@Test(priority= 2, description="Lib Prep - POST-PCR ", dependsOnMethods = { "Clarity_Login" })
	  @Test(priority= 9, description="Lib Prep - POST-PCR" ,dependsOnMethods = { "Clarity_LibPrep_PCR" })
	  public  void Clarity_LibPrep_POST_PCR() throws Exception {
		  ReportManager.startTestCase("T21 Lib Prep: POST-PCR","Clarity T21 Library Prep: POST-PCR");
		  
		      ClarityNavigation.T21Protocol.LibPrep.step(LIB_PREP_STEPS.POST_PCR);
		      LibPrepPostPCRPage.IceBucketLibPrepPage.addItem(ClarityDataManager.getPropertyValue(ClarityData.LIB_PREP_PCR_PLATE));
			  LibPrepPostPCRPage.VIEW_ICE_BUCKET();
			  //LibPrepPostPCRPage.IceBucketContentsPage.setDestinationContainer();
			  LibPrepPostPCRPage.BEGIN_WORK();
			  LibPrepPostPCRPage.PlaceSamplesPage.renameSTKPlateNameWithAutoValue();
			  LibPrepPostPCRPage.RECORD_DETAILS();
		  
			  
			  LibPrepPostPCRPage.RecordDetailsPage.fillUpPage("Z", ClarityDataManager.getPropertyValue(ClarityData.LIB_PREP_PCR_PLATE),ClarityDataManager.getPropertyValue(ClarityData.LIB_PREP_STK_PLATE) ,"");
			  //LibPrepPostPCRPage.RecordDetailsPage.fillUpPage(ClarityDataManager.Settings.getXLSDataSourceFile(),"LibPrep_POST_PCR",3);
			  ClarityDataManager.LibPrepPCR.FileGeneration.generatePCRFile("Lib_Prep_Configuration", 1);
			  LibPrepPostPCRPage.RecordDetailsPage.validateZephyrFile();
			  LibPrepPostPCRPage.RecordDetailsPage.checkLibPrepPreCSVStatus();
			  
			  LibPrepPostPCRPage.NEXT_STEPS();
			  //LibPrepBuildPlatePage.NextStepsPage.selectNextStep(sNextStep);
			   LibPrepBuildPlatePage.FINISH_STEP();
		  ReportManager.endTestCase();
	  }//Clarity_LibPrep_Build_Plate
	  
	  
	 // @Test(priority= 2, description="Extraction Protocol -Step 7", dependsOnMethods = { "Clarity_Login" })
	 @Test(priority= 10, description="Lib Prep  Sample Batch Failure" ,dependsOnMethods = { "Clarity_LibPrep_POST_PCR" })
	  public  void Clarity_LibPrep_SampleBatch_Failure() throws Exception {
		  ReportManager.startTestCase("Navigation","Navigate to Step 7");
		  ClarityNavigation.T21Protocol.LibPrep.step(LIB_PREP_STEPS.SAMPLE_FAILURE);
		  ReportManager.startTestCase("T21 Lib Prep","Clarity_Extraction_SampleBatch_Failure");
			 LibPrepSampleBatchFailurePage.IceBucketExtractionPage.addItem(ClarityDataManager.getPropertyValue(ClarityData.LIB_PREP_STK_PLATE));  
			 LibPrepSampleBatchFailurePage.VIEW_ICE_BUCKET();
			 LibPrepSampleBatchFailurePage.BEGIN_WORK();
			 LibPrepSampleBatchFailurePage.RecordDetailsPage.fillStepDetails();
			 LibPrepSampleBatchFailurePage.NEXT_STEPS();
			 LibPrepSampleBatchFailurePage.FINISH_STEP();
		  ReportManager.endTestCase();
	  }//Clarity_Extraction_SampleBatch_Failure
		  
	 
	 
	 //@Test(priority= 2, description="Lib Prep - Build Plate ", dependsOnMethods = { "Clarity_Login" })
	 @Test(priority= 11, description="Clarity_LibQC_X_Dilution" ,dependsOnMethods = { "Clarity_LibPrep_SampleBatch_Failure" })
	  public  void Clarity_LibQC_X_Dilution() throws Exception {
		  ReportManager.startTestCase("T21 Lib QC ","Clarity QC X Dilution");
		      ClarityNavigation.T21Protocol.LibQC.step(LIB_QC_STEPS.DILUTION);
		      LibQC20xDilutionPage.IceBucketLibPrepPage.addItem(ClarityDataManager.getPropertyValue(ClarityData.LIB_PREP_STK_PLATE));
			  LibQC20xDilutionPage.VIEW_ICE_BUCKET();
			  //LibQC20xDilutionPage.IceBucketContentsPage.setDestinationCointainer();
			  LibQC20xDilutionPage.BEGIN_WORK();
			  //LibQC20xDilutionPage.PlaceSamplesPage.renameContainerWithValues
			  //LibQC20xDilutionPage.PlaceSamplesPage.renameContainerWithValues("WRK_01112016", "CH_LABCHIP_1", "CH_LABCHIP_2","CH_LABCHIP_3");
			  LibQC20xDilutionPage.PlaceSamplesPage.renameContainerNameWithAutoValues();
			  LibQC20xDilutionPage.RECORD_DETAILS();
			  //LibQC20xDilutionPage.RecordDetailsPage.fillUpPage("sPOST_PCR_Instrument", "scanSTKPlate", "scanWRKPlate", "scanQCPlateA2", "scanQCPlateB2", "scanQCPlateC2", "sComments");
			  LibQC20xDilutionPage.RecordDetailsPage.fillUpPage(ClarityDataManager.Settings.getXLSDataSourceFile(),"LibQC_Dilution",3);
			  
			  ClarityDataManager.LibQC.FileGeneration.generate20XFile("Lib_QC_Configuration", 1);
			  
			  
			  LibQC20xDilutionPage.RecordDetailsPage.validate20X();
			  LibQC20xDilutionPage.RecordDetailsPage.check20XStatus();
			  LibQC20xDilutionPage.NEXT_STEPS();
			  //LibQC20xDilutionPage.NextStepsPage.selectNextStep(sNextStep);
			   LibQC20xDilutionPage.FINISH_STEP();
		  ReportManager.endTestCase();
	  }//Clarity_LibQC_X_Dilution
 	  
	  
	  
 
	 
	  //@Test(priority= 2, description="Lib Prep - Pre-PCR ", dependsOnMethods = { "Clarity_Login" })
	  @Test(priority= 12, description="Lib QC- Chip Prep" ,dependsOnMethods = { "Clarity_LibQC_X_Dilution" })
	  public  void Clarity_LibQC_ChipPrep() throws Exception {

		  ReportManager.startTestCase("T21 Lib QC ","Clarity QC Chip Prep");
	      ClarityNavigation.T21Protocol.LibQC.step(LIB_QC_STEPS.CHIP_PREP);
		  LibQCChipPrepPage.IceBucketPrepPage.addItem(ClarityDataManager.getPropertyValue(ClarityData.LIB_QC_QC_PLATE_A2));  
		  LibQCChipPrepPage.IceBucketPrepPage.addItem(ClarityDataManager.getPropertyValue(ClarityData.LIB_QC_QC_PLATE_B2));
		  LibQCChipPrepPage.IceBucketPrepPage.addItem(ClarityDataManager.getPropertyValue(ClarityData.LIB_QC_QC_PLATE_C2));
			  LibQCChipPrepPage.VIEW_ICE_BUCKET();
			  LibQCChipPrepPage.BEGIN_WORK();
			  //LibQCChipPrepPage.RecordDetailsPage.fillUpPage();
			  LibQCChipPrepPage.RecordDetailsPage.fillUpPage(ClarityDataManager.Settings.getXLSDataSourceFile(),"LibQC_Chip_Prep",2);
			  LibQCChipPrepPage.NEXT_STEPS();
			  //LibQCChipPrepPage.NextStepsPage.selectNextStep("");
			  LibQCChipPrepPage.FINISH_STEP();
		  ReportManager.endTestCase();
	  }//Clarity_LibQC_ChipPrep

	  
	  
	  //@Test(priority= 2, description="T21 Lib QC- Chip Loading", dependsOnMethods = { "Clarity_Login" })
	  @Test(priority= 13, description="T21 Lib QC- Chip Loading" ,dependsOnMethods = { "Clarity_LibQC_ChipPrep" })
	  public  void Clarity_LibQC_ChipLoading() throws Exception {

		  ReportManager.startTestCase("T21 Lib QC ","Clarity QC Chip Loading - LabChip 1");
	      ClarityNavigation.T21Protocol.LibQC.step(LIB_QC_STEPS.LAB_CHIP_LOADING);
	      //LibQCChipLoadingPage.IceBucketPrepPage.addItem("CH_LABCHIP_1");
	      LibQCChipLoadingPage.IceBucketPrepPage.addItem(ClarityDataManager.getPropertyValue(ClarityData.LIB_QC_QC_PLATE_A2));
			  LibQCChipLoadingPage.VIEW_ICE_BUCKET();
			  LibQCChipLoadingPage.BEGIN_WORK();
			  //LibQCChipLoadingPage.RecordDetailsPage.fillUpPage();
			  LibQCChipLoadingPage.RecordDetailsPage.fillUpPage(ClarityDataManager.Settings.getXLSDataSourceFile(),"LibQC_ChipLoad",2);
			  LibQCChipLoadingPage.NEXT_STEPS();
			  //LibQCChipLoadingPage.NextStepsPage.selectNextStep("");
			  LibQCChipLoadingPage.FINISH_STEP();
		  ReportManager.endTestCase();
		  
		  ReportManager.startTestCase("T21 Lib QC ","Clarity QC Chip Loading - LabChip 2");
	      ClarityNavigation.T21Protocol.LibQC.step(LIB_QC_STEPS.LAB_CHIP_LOADING);
	      //LibQCChipLoadingPage.IceBucketPrepPage.addItem("CH_LABCHIP_2");
	      LibQCChipLoadingPage.IceBucketPrepPage.addItem(ClarityDataManager.getPropertyValue(ClarityData.LIB_QC_QC_PLATE_B2));
			  LibQCChipLoadingPage.VIEW_ICE_BUCKET();
			  LibQCChipLoadingPage.BEGIN_WORK();
			  //LibQCChipLoadingPage.RecordDetailsPage.fillUpPage();
			  LibQCChipLoadingPage.RecordDetailsPage.fillUpPage(ClarityDataManager.Settings.getXLSDataSourceFile(),"LibQC_ChipLoad",2);
			  LibQCChipLoadingPage.NEXT_STEPS();
			  //LibQCChipLoadingPage.NextStepsPage.selectNextStep("");
			  LibQCChipLoadingPage.FINISH_STEP();
		  ReportManager.endTestCase();

		  ReportManager.startTestCase("T21 Lib QC ","Clarity QC Chip Loading - LabChip 3");
	      ClarityNavigation.T21Protocol.LibQC.step(LIB_QC_STEPS.LAB_CHIP_LOADING);
	      //LibQCChipLoadingPage.IceBucketPrepPage.addItem("CH_LABCHIP_3");
	      LibQCChipLoadingPage.IceBucketPrepPage.addItem(ClarityDataManager.getPropertyValue(ClarityData.LIB_QC_QC_PLATE_C2));
			  LibQCChipLoadingPage.VIEW_ICE_BUCKET();
			  LibQCChipLoadingPage.BEGIN_WORK();
			  //LibQCChipLoadingPage.RecordDetailsPage.fillUpPage("C:\\temp\\TestDataSamples.xlsx","LibQC_ChipLoad",1);
			  LibQCChipLoadingPage.RecordDetailsPage.fillUpPage(ClarityDataManager.Settings.getXLSDataSourceFile(),"LibQC_ChipLoad",2);
			  LibQCChipLoadingPage.NEXT_STEPS();
			  //LibQCChipLoadingPage.NextStepsPage.selectNextStep("");
			  LibQCChipLoadingPage.FINISH_STEP();
		  ReportManager.endTestCase();
	  }//Clarity_LibQC_ChipPrep
	
	
	  
	  //@Test(priority= 2, description="Extraction Protocol -Step 7", dependsOnMethods = { "Clarity_Login" })
	  @Test(priority= 14, description="LabChip Analysis / Sample Failure" ,dependsOnMethods = { "Clarity_LibQC_ChipLoading" })
	  public  void Clarity_LibQC_Analysys_Sample_Batch_Failure() throws Exception {
		  ReportManager.startTestCase("T21 Lib QC","T21 Library QC: LabChip Analysis / Sample Failure");
		     ClarityNavigation.T21Protocol.LibQC.step(LIB_QC_STEPS.LAB_CHIP_ANALYSIS_SAMPLE_FAILURE);
		     //LibQCLabChipAnalysisSampleFailure.IceBucketExtractionPage.addItem("WRK_01112016");
		     LibQCLabChipAnalysisSampleFailure.IceBucketExtractionPage.addItem(ClarityDataManager.getPropertyValue(ClarityData.LIB_QC_SCAN_WRK_PLATE));
			 LibQCLabChipAnalysisSampleFailure.VIEW_ICE_BUCKET();
			 LibQCLabChipAnalysisSampleFailure.BEGIN_WORK();
			 LibQCLabChipAnalysisSampleFailure.RecordDetailsPage.fillStepDetails();
			 ClarityDataManager.LibQC.FileGeneration.generateLabChipFiles("Lib_QC_Configuration", 1);
			 LibQCLabChipAnalysisSampleFailure.RecordDetailsPage.validateImportWellTable();
			 LibQCLabChipAnalysisSampleFailure.RecordDetailsPage.checkImportWellTableStatus();
			 LibQCLabChipAnalysisSampleFailure.NEXT_STEPS();
			 LibQCLabChipAnalysisSampleFailure.FINISH_STEP();
		  ReportManager.endTestCase();
	  }//Clarity_Extraction_SampleBatch_Failure

	  
	  
	  //@Test(priority= 2, description="Normalization", dependsOnMethods = { "Clarity_Login" })
	  @Test(priority= 15, description="Normalization" ,dependsOnMethods = { "Clarity_LibQC_Analysys_Sample_Batch_Failure" })
	  public  void Clarity_Normalization() throws Exception {
		  ReportManager.startTestCase("T21 Normalization- Data Analysis: ","Clarity T21 Normalization");
		      ClarityNavigation.T21Protocol.NormToDataAnalysis.step(NORM_TO_DA_STEPS.S1_NORMALIZATION);
		      //NormalizationPage.IceBucketLibPrepPage.addItem("WRK_01112016");
		      NormalizationPage.IceBucketLibPrepPage.addItem(ClarityDataManager.getPropertyValue(ClarityData.LIB_QC_SCAN_WRK_PLATE));
			  NormalizationPage.VIEW_ICE_BUCKET();
			  //Normalization.IceBucketContentsPage.setDestinationContainer();
			  NormalizationPage.BEGIN_WORK();
			  //NormalizationPage.PlaceSamplesPage.renameContainer("NRM_CH01112015");
			  NormalizationPage.PlaceSamplesPage.renameContainer();
			  NormalizationPage.RECORD_DETAILS();
			  //NormalizationPage.RecordDetailsPage.fillUpPage("C:\\temp\\TestDataSamples.xlsx","Normalization",1);
			  NormalizationPage.RecordDetailsPage.fillUpPage(ClarityDataManager.Settings.getXLSDataSourceFile(),"Normalization",1);
			  NormalizationPage.RecordDetailsPage.validateCalcDaughters();
			  NormalizationPage.RecordDetailsPage.checkCalculateDaughters();
			  NormalizationPage.RecordDetailsPage.validateCreateNORMCSV();
			  NormalizationPage.RecordDetailsPage.checkGenerateNorm();
			  NormalizationPage.NEXT_STEPS();
			  //LibPrepBuildPlatePage.NextStepsPage.selectNextStep(sNextStep);
			   LibPrepBuildPlatePage.FINISH_STEP();
		  ReportManager.endTestCase();
	  }//Clarity_Normalization

		

	  
	  //@Test(priority= 2, description="Extraction Protocol -Step 7", dependsOnMethods = { "Clarity_Login" })
	  @Test(priority= 16, description="Lib Prep  Sample Batch Failure" ,dependsOnMethods = { "Clarity_Normalization" })
	  //@Test(priority= 5, description="Lib Prep  Sample Batch Failure" ,dependsOnMethods = { "Clarity_Normalization" })
	  public  void Clarity_PlexingAssignment() throws Exception {
	    ReportManager.startTestCase("T21 Normalization- Plexing Assignment: ","Clarity T21 Plexing Assignment");
	        ClarityNavigation.T21Protocol.NormToDataAnalysis.step(NORM_TO_DA_STEPS.S2_PLEXING);
	        PlexingAssignmentPage.IceBucketExtractionPage.addItem(ClarityDataManager.getPropertyValue(ClarityData.NORM_NRM_PLATE));
	        //PlexingAssignmentPage.IceBucketExtractionPage.addItem("NRM_CH01112015");  
			 PlexingAssignmentPage.VIEW_ICE_BUCKET();
			 PlexingAssignmentPage.BEGIN_WORK();
			 PlexingAssignmentPage.RECORD_DETAILS();
			 // TODO Add the Select Plexing Option 
			 // and verify 
			 PlexingAssignmentPage.RecordDetailsPage.fillStepDetails();
			 PlexingAssignmentPage.NEXT_STEPS();
			 PlexingAssignmentPage.FINISH_STEP();
		  ReportManager.endTestCase();
	  }//Clarity_PlexingAssignment
	
	  
	  
	  
	  
	  //@Test(priority= 2, description="Clarity_High_Output_Multiplexing", dependsOnMethods = { "Clarity_Login" })
	  @Test(priority= 17, description="Clarity_High_Output_Multiplexing" ,dependsOnMethods = { "Clarity_PlexingAssignment" })
	  //@Test(priority= 6, description="Clarity_High_Output_Multiplexing" ,dependsOnMethods = { "Clarity_PlexingAssignment" })
	  public  void Clarity_High_Output_Multiplexing() throws Exception {
	    ReportManager.startTestCase("T21 Normalization- T21 High-Output: Multiplexing ","Clarity T21 Multiplexing ");
	    
	    
	    ClarityNavigation.T21Protocol.NormToDataAnalysis.step(NORM_TO_DA_STEPS.S3_HO_MULTIPLEXING);
		  //HOMultiplexingPage.IceBucketExtractionPage.addItem("NRM_CH01112015");
	    HOMultiplexingPage.IceBucketExtractionPage.addItem(ClarityDataManager.getPropertyValue(ClarityData.NORM_NRM_PLATE));
	    //HOMultiplexingPage.IceBucketExtractionPage.addALL_Ctrls();		  
	     HOMultiplexingPage.IceBucketExtractionPage.addControlSample(MULTIPLEXING_CRTLS.CTRL_3MB);
	     HOMultiplexingPage.IceBucketExtractionPage.addControlSample(MULTIPLEXING_CRTLS.CTRL_48MB);
	     HOMultiplexingPage.IceBucketExtractionPage.addControlSample(MULTIPLEXING_CRTLS.CTRL_7MB);
	     HOMultiplexingPage.IceBucketExtractionPage.addControlSample(MULTIPLEXING_CRTLS.CTRL_NPF);
		  
		  HOMultiplexingPage.VIEW_ICE_BUCKET();
		  HOMultiplexingPage.IceBucketContentsPage.replicateCtrl(MULTIPLEXING_CRTLS.CTRL_3MB, 8 );
		  HOMultiplexingPage.IceBucketContentsPage.replicateCtrl(MULTIPLEXING_CRTLS.CTRL_48MB, 8 );
		  HOMultiplexingPage.IceBucketContentsPage.replicateCtrl(MULTIPLEXING_CRTLS.CTRL_7MB, 8 );
		  HOMultiplexingPage.IceBucketContentsPage.replicateCtrl(MULTIPLEXING_CRTLS.CTRL_NPF, 8 );
		  
		  HOMultiplexingPage.BEGIN_WORK();
		  HOMultiplexingPage.PLACE_SAMPLES();
		  //HOMultiplexingPage.PlaceSamples.renameContainerName("TMP_CH01112016");
		  HOMultiplexingPage.PlaceSamples.renameContainerName();
		  
		  HOMultiplexingPage.RECORD_DETAILS();
		  //HOMultiplexing.RecordDetailsPage.fillUpPage();
		  
		  HOMultiplexingPage.RecordDetailsPage.fillUpPage(ClarityDataManager.Settings.getXLSDataSourceFile(),"Multiplexing",3);
		  HOMultiplexingPage.RecordDetailsPage.performcCoC();
		  HOMultiplexingPage.RecordDetailsPage.validateCOC();
		  HOMultiplexingPage.RecordDetailsPage.checkCOCStatus();
		  HOMultiplexingPage.NEXT_STEPS();
		  HOMultiplexingPage.FINISH_STEP();
	  }//Clarity_High_Output_Multiplexing 
//
	  
	  
	  
	  //@Test(priority= 2, description="Clarity_HOClusteringType", dependsOnMethods = { "Clarity_Login" })
	  @Test(priority= 18, description="Clarity_HO ClusteringType" ,dependsOnMethods = { "Clarity_High_Output_Multiplexing" })
	  //@Test(priority= 7, description="Clarity_HO ClusteringType" ,dependsOnMethods = { "Clarity_High_Output_Multiplexing" })
	  public  void Clarity_HO_ClusteringType() throws Exception {
	    ReportManager.startTestCase("T21 Normalization- HO: Clustering Type: ","T21 High-Output: Clustering Type");
	        ClarityNavigation.T21Protocol.NormToDataAnalysis.step(NORM_TO_DA_STEPS.S5_HO_CLUSTERING_TYPES); //S4_CLUSTERING_TYPES 
	        //HOClusteringTypePage.IceBucketExtractionPage.addItem("TMP_CH01112016");
	        HOClusteringTypePage.IceBucketExtractionPage.addItem(ClarityDataManager.getPropertyValue(ClarityData.T21HO_STP_NAME));
	        HOClusteringTypePage.VIEW_ICE_BUCKET();
	        HOClusteringTypePage.BEGIN_WORK();
	        //HOClusteringTypePage.RecordDetailsPage.fillUpPage(FLOW_CELL_ASSIGNMENT_TYPE.CLUSTERING);
	        HOClusteringTypePage.RecordDetailsPage.fillUpPage(ClarityDataManager.Settings.getXLSDataSourceFile(),"HOClusteringType",1);
	        HOClusteringTypePage.NEXT_STEPS();
	        HOClusteringTypePage.FINISH_STEP();
		  ReportManager.endTestCase();
	  }//Clarity_HO_ClusteringType
	  
		
	  
	  
	  //@Test(priority= 2, description="Normalization", dependsOnMethods = { "Clarity_Login" })
	  @Test(priority= 19, description="Normalization" ,dependsOnMethods = { "Clarity_HO_ClusteringType" })
	  //@Test(priority= 8, description="Normalization" ,dependsOnMethods = { "Clarity_HO_ClusteringType" })
	  public  void Clarity_HO_Clustering() throws Exception {
		  ReportManager.startTestCase("T21 Normalization- Clustering: ","Clarity T21 Clustering");

		      ClarityNavigation.T21Protocol.NormToDataAnalysis.step(NORM_TO_DA_STEPS.S7_HO_CLUSTERING); //S6_HO_CLUSTERING
		      //HOClusteringPage.IceBucketExtractionPage.addItem("TMP_CH01112016");
		      HOClusteringPage.IceBucketExtractionPage.addItem(ClarityDataManager.getPropertyValue(ClarityData.T21HO_STP_NAME));
			  HOClusteringPage.VIEW_ICE_BUCKET();
			  //HOClusteringPage.IceBucketContentsPage.setDestinationContainer();
			  HOClusteringPage.BEGIN_WORK();
			  //HOClusteringPage.PlaceSamplesPage.renameContainer("FCCH01112015");
			  HOClusteringPage.PlaceSamplesPage.renameContainer(CONTAINER.CLUSTERING);
			  HOClusteringPage.RECORD_DETAILS();
			  HOClusteringPage.RecordDetailsPage.fillUpPage(ClarityDataManager.Settings.getXLSDataSourceFile(),"HOClustering",2);
			  
			  HOClusteringPage.RecordDetailsPage.generateSampleSheet();
			  HOClusteringPage.RecordDetailsPage.checkSampleSheetStatus();
			  
			  HOClusteringPage.NEXT_STEPS();
			  //HOClusteringPage.NextStepsPage.selectNextStep(sNextStep);
			  HOClusteringPage.FINISH_STEP();
		  ReportManager.endTestCase();
	  }//Clarity_HO_Clustering
	  
	  
	  
	  //@Test(priority= 2, description="Extraction Protocol -Step 7", dependsOnMethods = { "Clarity_Login" })
	  @Test(priority= 20, description="Norm Clarity_HO_Sequencing_Prep" ,dependsOnMethods = { "Clarity_HO_Clustering" })
	  //@Test(priority= 9, description="Norm Clarity_HO_Sequencing_Prep" ,dependsOnMethods = { "Clarity_HO_Clustering" })
	  public  void Clarity_HO_Sequencing() throws Exception {
	    ReportManager.startTestCase("T21 Normalization- HO_Sequencing_Prep: ","Clarity T21 HO_Sequencing_Prep");
	        ClarityNavigation.T21Protocol.NormToDataAnalysis.step(NORM_TO_DA_STEPS.S8_HO_SEQUENCING_PREP); //S7_HO_SEQUENCING_PREP
	        //HOSequencingPage.IceBucketExtractionPage.addItem("FCCH01112015");
	        
	        HOSequencingPage.IceBucketExtractionPage.addItem(ClarityDataManager.getPropertyValue(ClarityData.T21HO_FC_NAME));
			 HOSequencingPage.VIEW_ICE_BUCKET();
			 HOSequencingPage.BEGIN_WORK();
//			 HOSequencingPage.RecordDetailsPage.fillUpPage();
			 HOSequencingPage.RecordDetailsPage.fillUpPage(ClarityDataManager.Settings.getXLSDataSourceFile(),"HOSeqPrep",2);
			 HOSequencingPage.NEXT_STEPS();
			 HOSequencingPage.FINISH_STEP();
		  ReportManager.endTestCase();
	  }//Clarity_HO_Sequencing
	  
	  
	  //@Test(priority= 2, description="Extraction Protocol -Step 7", dependsOnMethods = { "Clarity_Login" })
	  @Test(priority= 21, description="Norm Clarity_HO_Load_Flow_Cell" ,dependsOnMethods = { "Clarity_HO_Sequencing" })
	  //@Test(priority= 10, description="Norm Clarity_HO_Load_Flow_Cell" ,dependsOnMethods = { "Clarity_HO_Sequencing" })
	  public  void Clarity_HO_Load_Flow_Cell() throws Exception {
	    ReportManager.startTestCase("T21 Normalization- Clarity_HO_Load_Flow_Cell: ","Clarity T21 HO_Load_Flow_Cell");
	        ClarityNavigation.T21Protocol.NormToDataAnalysis.step(NORM_TO_DA_STEPS.S9_HO_LOAD_FLOW_CELL); //S8_HO_LOAD_FLOW_CELL
	        //HOLoadFlowCellPage.IceBucketExtractionPage.addItem("FCCH01112015");
	        HOLoadFlowCellPage.IceBucketExtractionPage.addItem(ClarityDataManager.getPropertyValue(ClarityData.T21HO_FC_NAME));
			 HOLoadFlowCellPage.VIEW_ICE_BUCKET();
			 HOLoadFlowCellPage.BEGIN_WORK();
			 HOLoadFlowCellPage.RecordDetailsPage.fillUpPage();
			 //HOLoadFlowCellPage.RecordDetailsPage.fillUpPage("C:\\temp\\TestDataSamples.xlsx","LoadFlowCell",1);
			 HOLoadFlowCellPage.NEXT_STEPS();
			 HOLoadFlowCellPage.FINISH_STEP();
		  ReportManager.endTestCase();
	  }//Clarity_HO_Load_Flow_Cell

	  
	  //@Test(priority= 2, description="T21 Sequencing- Clarity T21 Sequencing First Report", dependsOnMethods = { "Clarity_Login" })
	  @Test(priority= 22, description="T21 Sequencing- Clarity T21 Sequencing First Report" ,dependsOnMethods = { "Clarity_HO_Load_Flow_Cell" })
	  //@Test(priority= 11, description="T21 Sequencing- Clarity T21 Sequencing First Report" ,dependsOnMethods = { "Clarity_HO_Load_Flow_Cell" })
	  public  void Clarity_Seq_Run_Monitoring_First_Base_Report() throws Exception {
	    ReportManager.startTestCase("T21 Sequencing- Clarity T21 Sequencing First Report: ","Clarity T21 Sequencing: Run Monitoring: First Base Report");
	        ClarityNavigation.T21Protocol.NormToDataAnalysis.step(NORM_TO_DA_STEPS.S17_SEQ_RM_FIRST_BASE_REPORT); //S9_SEQ_RM_FIRST_BASE_REPORT
	        //SeqRunMonitoringFirstBaseReportPage.IceBucketExtractionPage.addItem("FCCH01112015");
	        SeqRunMonitoringFirstBaseReportPage.IceBucketExtractionPage.addItem(ClarityDataManager.getPropertyValue(ClarityData.T21HO_FC_NAME));
			 SeqRunMonitoringFirstBaseReportPage.VIEW_ICE_BUCKET();
			 SeqRunMonitoringFirstBaseReportPage.BEGIN_WORK();
			 //SeqRunMonitoringFirstBaseReportPage.RecordDetailsPage.fillUpPage(QC_STATUS.PASS);
			 //SeqRunMonitoringFirstBaseReportPage.RecordDetailsPage.fillUpPage(QC_STATUS.FAIL);
			 SeqRunMonitoringFirstBaseReportPage.RecordDetailsPage.fillUpPage(ClarityDataManager.Settings.getXLSDataSourceFile(),"SeqFirstBaseReport",1);
			 
			 ClarityDataManager.SeqRunMonitoringFirstBaseReport.FileGeneration.generateCBOTFile("Sequence_FirstReport_CBOT", 1);
			 SeqRunMonitoringFirstBaseReportPage.RecordDetailsPage.verifyCBotXMLFile();
			 SeqRunMonitoringFirstBaseReportPage.RecordDetailsPage.checkCBotStatus();
			 SeqRunMonitoringFirstBaseReportPage.NEXT_STEPS();
			 SeqRunMonitoringFirstBaseReportPage.NextStepsPage.selectNextStepAndApplyAll(SEQ_FIRST_BASE_REPORT_NEXT_STEP.RAW_CLUSTER_DENSITY);
			 SeqRunMonitoringFirstBaseReportPage.FINISH_STEP();
		  ReportManager.endTestCase();
	  }//Clarity_Seq_Run_Monitoring_First_Base_Report
	  
	  
	  
	  //@Test(priority= 2, description="Extraction Protocol -Step 7", dependsOnMethods = { "Clarity_Login" })
	  @Test(priority= 23, description="T21 Sequencing- Clarity T21 Sequencing RawClusterDensityPage" ,dependsOnMethods = { "Clarity_Seq_Run_Monitoring_First_Base_Report" })
	  //@Test(priority= 12, description="T21 Sequencing- Clarity T21 Sequencing RawClusterDensityPage" ,dependsOnMethods = { "Clarity_Seq_Run_Monitoring_First_Base_Report" })
	  public  void Clarity_Seq_Run_Monitoring_RawClusterDensity() throws Exception {
	    ReportManager.startTestCase("T21 Sequencing- Clarity T21 Sequencing Raw Cluster Density : ","Clarity T21 Sequencing: Run Monitoring: Raw Cluster Density ");
	        ClarityNavigation.T21Protocol.NormToDataAnalysis.step(NORM_TO_DA_STEPS.S18_SEQ_RM_RAW_CLUSTER_DENSITY);//S10_SEQ_RM_RAW_CLUSTER_DENSITY
	        //SeqRunMonitoringRawClusterDensityPage.IceBucketExtractionPage.addItem("FCCH01112015");
	        SeqRunMonitoringRawClusterDensityPage.IceBucketExtractionPage.addItem(ClarityDataManager.getPropertyValue(ClarityData.T21HO_FC_NAME));
			 SeqRunMonitoringRawClusterDensityPage.VIEW_ICE_BUCKET();
			 SeqRunMonitoringRawClusterDensityPage.BEGIN_WORK();
			 SeqRunMonitoringRawClusterDensityPage.RecordDetailsPage.fillUpPage();
			 //SeqRunMonitoringRawClusterDensityPage.RecordDetailsPage.fillUpPage("C:\\temp\\TestDataSamples.xlsx","SeqRawClusterDensity",1);
			 SeqRunMonitoringRawClusterDensityPage.RecordDetailsPage.verifyForReport();
			 SeqRunMonitoringRawClusterDensityPage.NEXT_STEPS();
			 SeqRunMonitoringRawClusterDensityPage.NextStepsPage.selectNextStepAndApplyAll(SEQ_RAW_CLUSTER_NEXT_STEP.PF_CLUSTER_DENSITY);
			 SeqRunMonitoringRawClusterDensityPage.FINISH_STEP();
		  ReportManager.endTestCase();
	  }//Clarity_Seq_Run_Monitoring_RawClusterDensity

	  
	  
	  //@Test(priority= 2, description="Extraction Protocol -Step 7", dependsOnMethods = { "Clarity_Login" })
	   @Test(priority= 24, description="T21 Sequencing- Clarity T21 Sequencing: Run Monitoring: PF Cluster Density" ,dependsOnMethods = { "Clarity_Seq_Run_Monitoring_RawClusterDensity" })
	   //@Test(priority= 13, description="T21 Sequencing- Clarity T21 Sequencing: Run Monitoring: PF Cluster Density" ,dependsOnMethods = { "Clarity_Seq_Run_Monitoring_RawClusterDensity" })
		  public  void Clarity_Seq_Run_Monitoring_PF_Cluster_Density() throws Exception {
		    ReportManager.startTestCase("T21 Sequencing- Clarity T21 Sequencing:Run Monitoring: PF Cluster Density: ","Clarity T21 Sequencing: T21 Sequencing: Run Monitoring: PF Cluster Density");
		        ClarityNavigation.T21Protocol.NormToDataAnalysis.step(NORM_TO_DA_STEPS.S19_SEQ_RM_PF_CLUSTER_DENSITY);//S11_SEQ_RM_PF_CLUSTER_DENSITY
		        //SeqRunMonitoringPFClusterDensityPage.IceBucketExtractionPage.addItem("FCCH01112015");  
		        SeqRunMonitoringPFClusterDensityPage.IceBucketExtractionPage.addItem(ClarityDataManager.getPropertyValue(ClarityData.T21HO_FC_NAME));
				 SeqRunMonitoringPFClusterDensityPage.VIEW_ICE_BUCKET();
				 SeqRunMonitoringPFClusterDensityPage.BEGIN_WORK();
				 SeqRunMonitoringPFClusterDensityPage.RecordDetailsPage.fillUpPage();
				 //SeqRunMonitoringPFClusterDensityPage.RecordDetailsPage.fillUpPage("C:\\temp\\TestDataSamples.xlsx","SeqPFClusterDensity",1);
				 SeqRunMonitoringPFClusterDensityPage.RecordDetailsPage.verifyForReport();
				 SeqRunMonitoringPFClusterDensityPage.NEXT_STEPS();
				 SeqRunMonitoringPFClusterDensityPage.NextStepsPage.selectNextStepAndApplyAll(SEQ_PF_CLUSTER_NEXT_STEP.CONTROL_ANALYSIS);
				 SeqRunMonitoringPFClusterDensityPage.FINISH_STEP();
			  ReportManager.endTestCase();
		  }//Clarity_Seq_Run_Monitoring_PF_Cluster_Density

		
	     //@Test(priority= 2, description="Extraction Protocol -Step 7", dependsOnMethods = { "Clarity_Login" })
		  @Test(priority= 25, description="T21 Sequencing- Clarity T21 Sequencing: Control Analysis" ,dependsOnMethods = { "Clarity_Seq_Run_Monitoring_PF_Cluster_Density" })
		  public  void Clarity_Seq_Control_Analysis() throws Exception {
		    ReportManager.startTestCase("T21 Sequencing- Clarity T21 Sequencing: Control Analysis: ","Clarity T21 Sequencing: Control Analysis");
		        ClarityNavigation.T21Protocol.NormToDataAnalysis.step(NORM_TO_DA_STEPS.S20_SEQ_CTRL_ANALYSIS); //S12_SEQ_CTRL_ANALYSIS
		        SeqControlAnalysisPage.IceBucketExtractionPage.addItem(ClarityDataManager.getPropertyValue(ClarityData.T21HO_FC_NAME));  
				 SeqControlAnalysisPage.VIEW_ICE_BUCKET();
				 SeqControlAnalysisPage.BEGIN_WORK();
				 SeqControlAnalysisPage.RecordDetailsPage.fillUpPage();
				 //SeqControlAnalysisPage.RecordDetailsPage.fillUpPage("C:\\temp\\TestDataSamples.xlsx","SeqCtrlAnalysis",1);
				 ClarityDataManager.DataAnalysis.FileGeneration.generateClassificationFile("T21_ClassificationFile", 1);
				 SeqControlAnalysisPage.RecordDetailsPage.importSequencingCtrls();
				 SeqControlAnalysisPage.RecordDetailsPage.checkCtrlsReviewStatus();
				 SeqControlAnalysisPage.NEXT_STEPS();
				 SeqControlAnalysisPage.NextStepsPage.selectNextStepAndApplyAll(CTRL_ANALYSIS_NEXT_STEP.PLATE_REPORT_ANALYSIS);
				 SeqControlAnalysisPage.FINISH_STEP();
			  ReportManager.endTestCase();
		  }//Clarity_Seq_Control_Analysis

	   	  */   
	     
	   @Test(priority= 2, description="T21 Data Analysis: Plate Report Analysis", dependsOnMethods = { "Clarity_Login" })
	    // @Test(priority= 26, description="T21 Data Analysis: Plate Report Analysis" ,dependsOnMethods = { "Clarity_Seq_Control_Analysis" })
		  public  void Clarity_DA_Plate_Report_Analysis() throws Exception {
		    ReportManager.startTestCase("T21 Data Analysis: Plate Report Analysis","T21 Data Analysis: Plate Report Analysis");
		        ClarityNavigation.T21Protocol.NormToDataAnalysis.step(NORM_TO_DA_STEPS.S22_DA_PLATE_REPORT_ANALYSIS);//S14_DA_PLATE_REPORT_ANALYSIS
		        DAPlateReportAnalysisPage.IceBucketExtractionPage.addItem(ClarityDataManager.getPropertyValue(ClarityData.T21HO_FC_NAME));   
				 DAPlateReportAnalysisPage.VIEW_ICE_BUCKET();
				 DAPlateReportAnalysisPage.BEGIN_WORK();
				 //DAPlateReportAnalysisPage.RecordDetailsPage.fillUpPage();
				 DAPlateReportAnalysisPage.RecordDetailsPage.fillUpPage(ClarityDataManager.Settings.getXLSDataSourceFile(),"DAPlateReportAnalysis",1);
				 DAPlateReportAnalysisPage.RecordDetailsPage.verifyPlateReport();
				 DAPlateReportAnalysisPage.RecordDetailsPage.checkPlateReviewStatus();
				 DAPlateReportAnalysisPage.NEXT_STEPS();
				 DAPlateReportAnalysisPage.NextStepsPage.selectNextStepAndApplyAll(DATA_ANALYSIS_NEXT_STEP.REFLEX_EVALUATION);
				 DAPlateReportAnalysisPage.FINISH_STEP();
			  ReportManager.endTestCase();
		  }//Clarity_Seq_Control_Analysis
	     
	     /*
	      * TODO Create Reflex Evaluation Page 
	      */

	  
	  
	  	 //@Test(priority= 2, description="T21 Data Analysis: Import Classification File", dependsOnMethods = { "Clarity_Login" })
	     @Test(priority= 27, description="T21 Data Analysis: Import Classification File" ,dependsOnMethods = { "Clarity_DA_Plate_Report_Analysis" })
		  public  void Clarity_DA_Import_Classification_File() throws Exception {
		    ReportManager.startTestCase("T21 Data Analysis: Import Classification File","T21 Data Analysis: Import Classification File");
		        ClarityNavigation.T21Protocol.NormToDataAnalysis.step(NORM_TO_DA_STEPS.S24_DA_IMPORT_CLASSIFICATION_FILE);
		        DAImportClassificationFilePage.IceBucketExtractionPage.addItem(ClarityDataManager.getPropertyValue(ClarityData.T21HO_FC_NAME));   
				 DAImportClassificationFilePage.VIEW_ICE_BUCKET();
				 DAImportClassificationFilePage.BEGIN_WORK();
				 //DAImportClassificationFilePage.RecordDetailsPage.fillUpPage();
				 DAImportClassificationFilePage.RecordDetailsPage.fillUpPage(ClarityDataManager.Settings.getXLSDataSourceFile(),"DAImportClassFile",1);
				 DAImportClassificationFilePage.RecordDetailsPage.importClassificationFile();
				 DAImportClassificationFilePage.RecordDetailsPage.checkImportFileStatus();
				 DAImportClassificationFilePage.NEXT_STEPS();
				 DAImportClassificationFilePage.NextStepsPage.selectNextStepAndApplyAll(DA_IMPORT_FILE_NEXT_STEP.MARK_AS_COMPLETE);		
				 //DAImportClassificationFilePage.FINISH_STEP();
			  ReportManager.endTestCase();
		  }//Clarity_DA_Import_Classification_File
		
		  
		
	  /*	   
	  @AfterMethod
	  public void tearDown() {
		  //WebDriverManager.quit();
	  }
	  */
	 
	  @AfterSuite(description="After Test Suite Method")
	      public void afterSuite() {
		      ReportManager.startTestCase("Execution");
	          ReportManager.log(REPORT_STATUS.LOG, "Workflow", "Done");
	          ReportManager.endTestCase();
	          //ReportManager.closeReport();
	      }

}//Clarity_GENOME_QAWorkflowIntegration
